package com.example.coroutinesflow

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.net.URL
import javax.net.ssl.HttpsURLConnection

var listUrl: List<String> = listOf(
    "https://www.google.com",
    "https://www.facebook.com",
    "https://www.github.com",
    "https://www.twitter.com",
    "https://www.instagram.com",
    "https://onlyfans.com",
    "https://rt.pornhub.com",
    "https://www.youtube.com",
    "https://vk.com",
    "https://teach.vibelab.ru"
)

fun main() {
    runBlocking {
        for (i in listUrl) {
            launch {
                try {
                    val connection = URL(i).openConnection() as HttpsURLConnection
                    connection.requestMethod = "HEAD"
                    connection.connectTimeout = 5000
                    connection.readTimeout = 5000
                    toString2(i, connection.responseCode == HttpsURLConnection.HTTP_OK)
                } catch (e: Exception) {
                    toString2(i, false)
                }
            }
        }
    }
}


private fun toString2(string: String, boolean: Boolean) {
    if (boolean) {
        println("Сайт ${string} доступен")
    } else {
        println("Сайт ${string} недоступен")
    }
}